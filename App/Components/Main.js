
var React = require('react-native');

var {
  ActivityIndicatorIOS,
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight
} = React;

var Dashboard = require('./Dashboard');
var api = require('../Utils/api');

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      isLoading: false,
      error: false
    };
  }

  handleChange(event) {
    this.setState({
      username: event.nativeEvent.text
    });
  }

  handleSubmit() {
    if (this.state.isLoading) {
      return;
    }

    // update our indicatorIOS spinner
    this.setState({
      isLoading: true
    });

    // get user data from github
    api.getBio(this.state.username)
      .then((res) => {
        if (res.message === 'Not Found') {
          this.setState({
            error: 'User not found',
            isLoading: false
          });
          return;
        }

        this.props.navigator.push({
          title: res.name || "Select an Option",
          component: Dashboard,
          passProps: {
            userInfo: res
          }
        });

        this.setState({
          isLoading: false,
          error: false,
          username: ''
        });
      })
      .catch((err) => {
        console.log(error);
      })
      .done();
  }

  render() {
    let showError = (
      this.state.error ? <Text>{this.state.error}</Text> : <View></View>
    );

    return (
      <View style={styles.mainContainer} >
        <Text style={styles.title}> Search for a Github User </Text>

        <TextInput
          style={styles.searchInput}
          value={this.state.username}
          onChange={this.handleChange.bind(this)}
        />

        <TouchableHighlight
          style={styles.button}
          onPress={this.handleSubmit.bind(this)}
          underlayColor="white" >
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableHighlight>

        <ActivityIndicatorIOS
          animating={this.state.isLoading}
          color="#111"
          size="large"
        />
        
        {showError}
      </View>
    );
  }
}

var styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#48BBEC',
    padding: 30,
    marginTop: 65
  },
  title: {
    marginBottom: 28,
    marginTop: 25,
    textAlign: 'center',
    color: 'white'
  },
  searchInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 8,
    color: 'white'
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center'
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

module.exports = Main;