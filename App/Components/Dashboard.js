
var React = require('react-native');

var {
	Image,
	TouchableHighlight,
	Text,
	View,
	StyleSheet
} = React;

class Dashboard extends React.Component {

	createButtonStyles(buttonType) {
		let buttonStyles = {
			flex: 1,
			flexDirection: 'row',
			alignSelf: 'stretch',
			justifyContent: 'center'
		};

		switch (buttonType) {
			case 'profile':
				buttonStyles.backgroundColor = '#48BBEC';
				break;
			case 'repos':
				buttonStyles.backgroundColor = '#E77AAE';
				break;
			case 'notes':
				buttonStyles.backgroundColor = '#758BF4';
				break;
		}

		return buttonStyles;
	}

	goToProfile() {
		console.log('Going to profile page');
	}

	goToRepos() {
		console.log('Going to repos');
	}

	goToNotes() {
		console.log('Going to notes');
	}

	render() {
		return (
			<View style={styles.container}>
				<Image 
					souce={{uri: this.props.userInfo.avatar_url}}
					style={styles.image}
				/>
				<TouchableHighlight
					style={this.createButtonStyles('profile')}
					onPress={this.goToProfile.bind(this)}
					underlayColor="#B8D4F5">
					<Text style={styles.buttonText}>View Profile</Text>
				</TouchableHighlight>
				<TouchableHighlight
					style={this.createButtonStyles('repos')}
					onPress={this.goToRepos.bind(this)}
					underlayColor="#B8D4F5">
					<Text style={styles.buttonText}>View Repos</Text>
				</TouchableHighlight>
				<TouchableHighlight
					style={this.createButtonStyles('notes')}
					onPress={this.goToNotes.bind(this)}
					underlayColor="#B8D4F5">
					<Text style={styles.buttonText}>View Notes</Text>
				</TouchableHighlight>
			</View>
		);
	}
}

var styles = StyleSheet.create({
  container: {
    marginTop: 65,
    flex: 1
  },
  image: {
    height: 350
  },
  buttonText: {
    fontSize: 24,
    color: 'white',
    alignSelf: 'center'
  }
});


module.exports = Dashboard;